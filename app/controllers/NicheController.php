<?php

class NicheController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($niche_slug,$sort_name='top',$product_id=null)
	{
		$thisNiche = Niche::where('slug',$niche_slug)->first();
		$niches = Niche::where('isHidden',0)->get();
		$thisSort = Sort::where('name',$sort_name)->first();
		$sorts = Sort::all();
		return View::make('niche',compact('thisNiche','niches','thisSort','sorts'));
	}

	public function showJson($niche_slug,$sort_name,$product_id=null){
		
		$orderBy = $sort_name=='top'?'wants':'id';
		
		$thisNiche = Niche::where('slug',$niche_slug)->first();
		$query = Product::where('imgurAlbum','!=','')
			->where('isHidden', 0)
			->orderBy($orderBy, 'desc');

		if($product_id){
			$query = $query->where('id','!=',$product_id);
		}

		if($niche_slug=='all')
			$query = $query->take(50);
		else
			$query = $query->where('niche_name',$thisNiche->name);
		

		$products = $query->get()->toArray();

		if($product_id){
			$product = Product::find($product_id);
			$product->isHighlighted=true;
			array_unshift($products,$product);
		}

		if($niche_slug=='all'){
			foreach($products as $key=>$product){
				$niche = $product['niche'];
				if($niche == null || $niche['isHidden']==1)
					unset($products[$key]);
			}
			$products = array_values($products);
		}

		return Response::json($products);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
