<?php

class SessionController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		/*

		$client = new OAuth2\Client(getenv('redditId'), getenv('redditSecret'));

		$code = Input::get( 'code' );

		$scopes = array('identity');
		if(Input::has( 'subscribe' )){
			$scopes = array('identity','subscribe');
		}

		$authUrl = 'https://ssl.reddit.com/api/v1/authorize';
		$tokenUrl = 'https://ssl.reddit.com/api/v1/access_token';

		if ($code) {
			
			$state = json_decode(urldecode(Input::get('state')));

		    if($state->subscribe){
		    	$scope = array('identity,subscribe');
		    }else{
		    	$scope = array('identity');
		    }
		    
		    $reddit->requestAccessToken($code, Input::get('state'),array('identity,subscribe'));
		    $me = json_decode($reddit->request('/api/v1/me.json'));

		    $user = User::firstOrCreate(['username'=>$me->name]);

		    if($state->subscribe){
		    	$user->isSubscribed = 1;
	    		$user->save();
		    }
	    	Auth::login($user);

	    	if($state->subscribe){
	    		$reddit->request('/api/subscribe','POST',array(
		    		'action'=>'sub',
		    		'sr'=>'t5_3354m'
		    	));
	    	}
	    	
		    return Redirect::to($state->referrer);
		}

		$stateJson = json_encode(array(
			'subscribe'=>Input::has( 'subscribe' ),
			'referrer'=>URL::previous(),
			'random'=>md5(uniqid(rand(), TRUE)),
		));

		$url = $client->getAuthenticationUrl($authorizeUrl, $redirectUrl, array("scope" => implode(',',$scopes), "state" => $stateJson));
		$url = filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED);

		return	Redirect::away($url);


		*/
		$CLIENT_ID     = getenv('redditId');
		$CLIENT_SECRET = getenv('redditSecret');

		$REDIRECT_URI           = getenv('loginUrl');
		$AUTHORIZATION_ENDPOINT = 'https://ssl.reddit.com/api/v1/authorize';
		$TOKEN_ENDPOINT         = 'https://ssl.reddit.com/api/v1/access_token';

		$client = new OAuth2\Client($CLIENT_ID, $CLIENT_SECRET, OAuth2\Client::AUTH_TYPE_AUTHORIZATION_BASIC);
		$headers = array(
			'User-Agent:'=>'nichekick'
		);

		if(Input::has( 'subscribe' ))
			$scopes = array('identity','subscribe');
		else
			$scopes = array('identity');

		if (!Input::get( 'code' )){

			$stateJson = json_encode(array(
				'subscribe'=>Input::has( 'subscribe' ),
				'referrer'=>URL::previous(),
				'random'=>md5(uniqid(rand(), TRUE)),
			));

		    $auth_url = $client->getAuthenticationUrl(
		    	$AUTHORIZATION_ENDPOINT
		    	,$REDIRECT_URI
		    	,array(
		    		'scope' => implode(',',$scopes)
		    		,'state' => $stateJson
		    	)
		    );
		    return Redirect::to($auth_url);
		}

	    $params = array('code' => Input::get( 'code' ), 'redirect_uri' => $REDIRECT_URI);
	    $response = $client->getAccessToken($TOKEN_ENDPOINT, 'authorization_code', $params);

	    $client->setAccessToken($response['result']['access_token']);
	    $client->setAccessTokenType(OAuth2\Client::ACCESS_TOKEN_BEARER);
	    $meResult = $client->fetch('https://oauth.reddit.com/api/v1/me.json',array(),'GET',$headers);
	    
	    $user = User::firstOrCreate(['username'=>$meResult['result']['name']]);

	    $state = json_decode(urldecode(Input::get('state')));
	    if($state->subscribe){
	    	$user->isSubscribed = 1;
	    }
	    $user->save();
    	Auth::login($user);

    	if($state->subscribe){
    		$client->fetch(
    			'https://oauth.reddit.com/api/subscribe'
    			,array(
		    		'action'=>'sub',
		    		'sr'=>'t5_3354m'
	    		)
    			,'POST'
    			,$headers
	    	);
    	}
    	
	    return Redirect::to($state->referrer);

		return $response;
		
		
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		Auth::logout();
		return Redirect::to(URL::previous());
	}


}
