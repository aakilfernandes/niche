
<!DOCTYPE html>
<html lang="en" class="{{$htmlClass}}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Cool stuff wanted by Reddit. Reddit powered shopping.">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">
    <title>NicheKick - Wanted by Reddit</title>
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap/bootstrap.css" rel="stylesheet">
    <link href="/css/bootstrap/bootstrap.green.css" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Black Ops One&subset=latin" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template -->
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="/css/style.css" rel="stylesheet">
    @yield('styles')
  </head>

  <body>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-54235704-1', 'auto');
      ga('send', 'pageview');

    </script>

    <div class="navbar navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/all/top">NicheKick <i class="fa fa-heart"></i></a>
        </div>
        <div class="navbar-collapse collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            @if(Auth::check())
              <li><a href="/wants">My Wants</a></li>
            @endif
          </ul>
          <div class="nav navbar-nav navbar-right">
            @if(Auth::check())
              <a href="/logout" class="btn btn-default navbar-btn">Logout</a>
            @else
              <a href="/login" class="btn btn-primary loginButton navbar-btn">Login</a>
            @endif
            <div class="visible-xs">
              <hr>
              @yield('navbarLinks')
            </div>
          </div>
        </div>
        <!--
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    @yield('content')
    @include('partial.loginModal')
    {{HTML::script('//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.min.js')}}
    {{HTML::script('//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js')}}
    {{HTML::script('//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.2.0/js/bootstrap.min.js')}}
    {{HTML::script('//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.2.0/js/tooltip.min.js')}}
    {{HTML::script('//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.6.0/underscore-min.js')}}
    {{HTML::script('//cdnjs.cloudflare.com/ajax/libs/backbone.js/1.1.2/backbone-min.js')}}
    {{HTML::script('/js/models/UserModel.js')}}
    {{HTML::script('/js/views/SidebarView.js')}}
    {{HTML::script('/js/script.js')}}
    @yield('scripts')
  </body>
</html>
