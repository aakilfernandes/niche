<script type="text/template" id="template-product">
<div class="product col-xs-12 col-xs-B-6 col-md-4 col-lg-3" >
	<div><div class="product-header">
		<span class="product-title"><%-title%></span>
	</div></div>
	<div class="product-display" <% if(urlText=='RedBubble'){%> data-toggle="tooltip" title="This design might be available on clothing, posters, cases, and more"<% }%>>
		<div class="product-images">
			<%_.times(images.length,function(){%>
				<a class="product-image" href="/product/<%-id%>/info" target="_blank"></a>
			<%})%>
		</div>
		<% if(images.length>1){%>
			<div class="product-bullets">
				<%_.times(images.length,function(index){%>
					<div class="product-bullet <%=(index==0)?'product-bullet-active':''%>"></div>
				<%})%>
			</div>
		<%}%>
	</div>
	<div class="product-footer">
		<span class="product-price"></span><span class="product-wants"></span>
	</div>
	<div class="product-buttons ">
		<a type="button" class="btn btn-info product-button-reddit" data-toggle="tooltip" title="Post to /r/<%-niche.subreddit%>" target="_blank" href="http://reddit.com/r/<%-niche.subreddit%>/submit?url=<%-encodeURIComponent('http://imgur.com/a/'+imgurAlbum)%>&title=<%-encodeURIComponent(title)%>"><i class="fa fa-reddit"></i></a>
		<a type="button" class="btn btn-primary product-button-imgur" data-toggle="tooltip" title="View Imgur Album" target="_blank" href="http://imgur.com/a/<%-imgurAlbum%>"><i class="fa fa-camera"></i></a>
		<a type="button" class="btn btn-default product-button-want" data-toggle="tooltip" title="Want"><i class="fa fa-heart"></i></a>
		<a type="button" class="btn btn-danger product-button-shop"  data-toggle="tooltip" title="View on <%-urlText%>" target="_blank" href="/product/<%-id%>/info"><i class="fa fa-shopping-cart"></i></a>
		<span class="product-buttons-hack"></span>
	<div>
</div>
</script>