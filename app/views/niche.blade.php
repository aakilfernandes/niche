@extends('layout',array('htmlClass'=>"niche niche-{$thisNiche->name}"))

@section('content')
<div class="container">
<div class="alert alert-info">
	<p>Cool stuff voted on by Redditors. Keep in touch by <a href='/login/?subscribe=1'>subscribing to /r/NicheKick</a></p>
</div>
<div class="row">
	<div class="hidden-xs col-sm-3 col-md-2"><div class="sidebar">
		@include('partial.sortLinks')
		@include('partial.nicheLinks')
	</div></div>
	<div class="col-sm-9 col-md-10">
		<div class="products row products-loading"></div>
	</div>
</div></div>
@if(!Auth::check())
<div class="banner hidden-xs"><div class="container">
	<a href="/login/?subscribe=1">Get {{$thisNiche->bannerName()}} stuff on your Reddit front page daily. Click here to subscribe to /r/NicheKick</a>
</div></div>
@endif
@stop

@section('navbarLinks')
	@include('partial.nicheLinks')
@stop

@section('styles')
	{{HTML::style('/css/partial/product.css')}}
@stop

@section('scripts')
	@include('template.product')
	{{HTML::script('/js/views/LazyView.js')}}
	{{HTML::script('/js/views/BannerView.js')}}
	{{HTML::script('/js/views/ProductView.js')}}
	{{HTML::script('/js/models/ProductModel.js')}}
	{{HTML::script('/js/collections/ProductsCollection.js')}}
	{{HTML::script('/js/page/niche.js')}}
@stop
