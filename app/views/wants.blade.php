@extends('layout',array('htmlClass'=>"wants"))

@section('content')
<div class="container"><div class="row">
	<div class="hidden-xs col-sm-3 col-md-2"><div class="sidebar">
		@include('partial.nicheLinks')
	</div></div>
	<div class="col-sm-9 col-md-10">
		<div class="alert noProductsMessage alert-info hide">
			<b>You haven't wanted any items yet.</b> Click the heart under a product and it'll show up here.
		</div>
		<div class="products row products-loading"></div>
	</div>
</div></div>
@stop

@section('navbarLinks')
	@include('partial.nicheLinks')
@stop

@section('styles')
	{{HTML::style('/css/partial/product.css')}}
@stop

@section('scripts')
	@include('template.product')
	{{HTML::script('/js/views/LazyView.js')}}
	{{HTML::script('/js/views/ProductView.js')}}
	{{HTML::script('/js/models/ProductModel.js')}}
	{{HTML::script('/js/collections/ProductsCollection.js')}}
	{{HTML::script('/js/page/wants.js')}}
@stop
