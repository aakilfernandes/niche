<ul class="nicheLinks">
@foreach($niches as $niche)
	@if($thisNiche && $niche->name==$thisNiche->name)
		<li class="nicheLink nicheLink-active">
			<a href="/{{$niche->slug}}/{{$thisSort->name}}">
				{{$niche->displayName}}
			</a>
		</li>
	@else
		<li class="nicheLink"><a href="/{{$niche->slug}}/{{$thisSort?$thisSort->name:'top'}}">{{$niche->displayName}}</a></li>
	@endif
@endforeach
</ul>