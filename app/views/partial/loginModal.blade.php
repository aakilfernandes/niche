<div class="modal fade loginModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Login with Reddit</h4>
      </div>
      <div class="modal-body">
        <p>We're about to send you to Reddit.com to authenticate your account. Would you like us to automatically subscribe you to /r/NicheKick?</p>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-info" href="/login/">Log in</a>
         <a type="button" class="btn btn-primary" href="/login/?subscribe=1">Log in + Subscribe</a>
      </div>
    </div>
  </div>
</div>