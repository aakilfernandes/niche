<ul>
@foreach($sorts as $sort)
	@if($thisSort && $sort->name==$thisSort->name)
		<li class="sortLink sortLink-active" >
			<a href="/{{$thisNiche->slug}}/{{$thisSort->name}}">
				{{$sort->displayName}}
			</a>
		</li>
	@else
		<li class="sortLink" >
			<a href="/{{$thisNiche?$thisNiche->slug:'all'}}/{{$sort->name}}">
				{{$sort->displayName}}
			</a>
		</li>
	@endif
@endforeach
</ul>