<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAlbumFieldToProducts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('products',function($table){
			$table->text('imgurImages')->after('images');
			$table->string('imgurAlbum')->after('images');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('products',function($table){
			$table->dropColumn('imgurAlbum');
			$table->dropColumn('imgurImages');
		});
	}

}
