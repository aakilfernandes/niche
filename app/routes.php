<?php

Route::group(array('before' => 'auth'), function()
{
	Route::resource('/logout','SessionController@destroy');

    Route::post('/product/{product_id}/wantToggle',function($product_id){
       	if(Auth::user()->hasProductWithId($product_id))
        	Auth::user()->products()->detach($product_id);
        else
        	Auth::user()->products()->save(Product::find($product_id));
    });

    Route::get('/wants',function(){
		$thisNiche = null;
		$niches = Niche::where('isHidden',0)->get();
		$thisSort = null;
		$sorts = Sort::all();

    	return View::make('wants',compact('thisNiche','niches','thisSort','sorts'));
    });

    Route::get('/wants.json', function()
	{
		return Response::json(Auth::user()->products);
	});
});

Route::get('/', function(){
	return Redirect::to('all/top',301);
});

Route::resource('/login','SessionController@create');

Route::get('/user.json', function()
{
	if(Auth::check())
		return Response::json(Auth::user());
	else
		return Response::make('No authorized user',403);
});

Route::get('/product/{product_id}/info', function($product_id)
{
	$product = Product::find($product_id);
	$url = 'http://go.redirectingat.com?'.http_build_query(array(
		'id'=>'72281X1521188'
		,'xs'=>1
		,'url'=>$product->url
	));
	return Redirect::away($url);
});

Route::get('/p/{product_id}/{product_title}',function($product_id){
	$product = Product::find($product_id);
	return Redirect::to("/{$product->niche->slug}/top/$product_id",301);
});


Route::get('/{niche_slug}/{sort_name}.json', 'NicheController@showJson');
Route::get('/{niche_slug}/{sort_name}/{product_id}.json', 'NicheController@showJson');

Route::get('/{niche_slug}/{sort_name}', 'NicheController@show');
Route::get('/{niche_slug}/{sort_name}/{product_id}', 'NicheController@show');