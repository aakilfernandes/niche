<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class HideProductsCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'niche:hideProducts';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Hide products not on the list of niches';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$products = Product::all();
		$niches = Niche::all();
		$niche_names = array();

		foreach($niches as $niche){
			array_push($niche_names,$niche->name);
		}

		foreach($products as $product){
			if(in_array($product->niche_name,$niche_names)){
				//$product->isHidden=0;
			}else{
				$product->isHidden=1;
			}
			$product->save();
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
