<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CreateAlbumsCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'niche:createAlbums';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Create imgur albums.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		if($this->argument('product_id')){
			$product = Product::find($this->argument('product_id'));
			$product ->refreshAlbum();
		}else{
			if($this->option('force'))
				$products = Product::all();
			else
				$products = Product::where('imgurAlbum','')->where('isHidden',0)->get();
	
			foreach($products as $product){
				try {
					$product->refreshAlbum();
				} catch (Exception $e) {
					$this->info("Exception on product {$product->id}: ".$e->getMessage());	
				}
			}
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('product_id', InputArgument::OPTIONAL, 'A product id.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('force', 'f', InputOption::VALUE_NONE, 'Force refresh'),
		);
	}

}
