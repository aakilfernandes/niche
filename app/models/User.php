<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	protected $table = 'users';
	
	protected $fillable = array('username','isSubscribed');

	protected $attributes = array(
	   'isSubscribed' => 0,
	);

	protected $hidden = array('password', 'remember_token');

	public function getWantedItemsAttribute(){
		return json_decode($this->wantedItems);
	}

	public function products() {
        return $this->belongsToMany('Product')->withTimestamps();    
    }

    public function hasProductWithId($product_id){
    	if (Auth::user()->products()->wherePivot('product_id',$product_id)->first())
    		return true;
    	else
	    	return false;
    }

}
