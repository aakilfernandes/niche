<?php

class Product extends \Eloquent {
	protected $fillable = [];
	protected $appends = array('niche','isWanted','imgurImages');
	protected $defaults = array(
		'isHighlighted'=>false
	);
	

	/*public function getImagesAttribute(){
		return explode(',',$this->imagesList);
	}
	*/
	public function getImagesAttribute(){
		return explode(' ',$this->attributes['images']);
	}
	public function setImagesAttribute($value){
	    $this->attributes['images'] = implode(' ',$value);
	}

	public function getImgurImagesAttribute(){
    	return json_decode($this->attributes['imgurImages']);

    	return array_map(function($id){
			return "http://i.imgur.com/$id.jpg";
		}, json_decode($this->attributes['imgurImages']));
	}
	public function setImgurImagesAttribute($value){
	    $this->attributes['imgurImages'] = json_encode($value);
	}

	public function getNicheAttribute($value){
    	return Niche::where('name',$this->niche_name)->first();
	}

	public function getIsWantedAttribute(){
		if(!Auth::check()) return false;

		return Auth::user()->hasProductWithId($this->id);
	}

	public function users() {
        return $this->belongsToMany('User')->withTimestamps();    
    }

    public function refreshAlbum(){
    	$albumResponse = Unirest::post("https://imgur-apiv3.p.mashape.com/3/album",
		  array(
		    "X-Mashape-Key" => getenv('mashapeKey'),
		    "Authorization" => "Client-ID ".getenv('imgurId')
		  ),
		  array(
		    "description" => $this->title
		  )
		);

		$imgurImages = array();
		$description = "{$this->title} - http://nichekick.com/p/{$this->id}/".Str::slug($this->title);
		foreach($this->images as $key=>$image){
			$imageResponse = Unirest::post("https://imgur-apiv3.p.mashape.com/3/image",
			  array(
			    "X-Mashape-Key" => getenv('mashapeKey'),
			    "Authorization" => "Client-ID ".getenv('imgurId')
			  ),
			  array(
			    "image" => $image
			    ,"album"=> $albumResponse->body->data->deletehash
			    ,'description'=>$description
			  )
			);
			if($imageResponse->code==200){
				array_push($imgurImages,$imageResponse->body->data->id);
			}else{
				$this->imgurAlbum = '';
				$this->imgurImages = $imgurImages;
				$this->save();
				throw new Exception("image $key: {$imageResponse->data->error}");
			};
		}

		$imageResponse = Unirest::post("https://imgur-apiv3.p.mashape.com/3/image",
		  array(
		    "X-Mashape-Key" => getenv('mashapeKey'),
		    "Authorization" => "Client-ID ".getenv('imgurId')
		  ),
		  array(
		    "image" => 'http://nichekick.com/img/logo.png'
		    ,"album"=> $albumResponse->body->data->deletehash
		    ,'description'=>'Wanted by Reddit - http://nichekick.com'
		  )
		);

		$this->imgurAlbum = $albumResponse->body->data->id;
		$this->imgurImages = $imgurImages;
		$this->save();

    }
}