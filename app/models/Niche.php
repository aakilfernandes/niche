<?php

class Niche extends \Eloquent {
	protected $fillable = [];

	public function bannerName(){
		if($this->name=='all')
			return 'NicheKick';
		else
			return $this->displayName;
	}
}