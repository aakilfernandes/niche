$document = $(document)
$html = $('html')
$body = $('body')
$loginModal = $('.loginModal')

sidebarView = new SidebarView

$.ajax({
	url:'/user.json',
  	statusCode: {
    	403: function() {
      		window.userModel = null
    	},200:function(user){
    		window.userModel = new UserModel(user)
    	}
  	}
});

$('.loginButton').click(function(event){

	event.preventDefault()
	event.stopPropagation()

	$('.loginModal').modal('show')

})


$('.nicheLink').click(function(){
  var $this = $(this)

  $('.products').addClass('products-loading')
  $body.scrollTop(1)

  $this.addClass('nicheLink-active')
  $this.siblings('.nicheLink-active').removeClass('nicheLink-active')
})

$('.sortLink').click(function(){
  var $this = $(this)

  $('.products').addClass('products-loading')
  $body.scrollTop(1)
  
  $this.addClass('sortLink-active')
  $this.siblings('.sortLink-active').removeClass('sortLink-active')
})