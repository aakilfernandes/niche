ProductModel = Backbone.Model.extend({
	initialize:function(){
		var that = this

		this.collection.modelCount = this.collection.modelCount?this.collection.modelCount+1:1
		this.productView = new ProductView({model:this})

		this.on('change:isWanted',function(productModel,isWanted){
			if(!userModel){
				this.attributes.isWanted=false
				$loginModal.modal('show')
				return;
			}
			this.productView.renderIsWanted()
			$.post('/product/'+this.get('id')+'/wantToggle/')
		})

	}
})