LazyView = Backbone.View.extend({
	template:$('#template-product').html()
	,initialize:function(options,$el,src,delay,callback){
		setTimeout(function(){
			$('<img>')
				.load(function(){
					$el.css('background-image','url('+src+')')
					callback()
				})
				.attr('src',src)
		},delay)
			
	}
});