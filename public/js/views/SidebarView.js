SidebarView = Backbone.View.extend({
	el:'.sidebar'
	,initialize:function(){
		var that = this
		$document.scroll(function(x){
			if($document.scrollTop()>=92){
				that.$el.addClass('sidebar-sticky')
			}else{
				that.$el.removeClass('sidebar-sticky')
			}
		})
	}
})