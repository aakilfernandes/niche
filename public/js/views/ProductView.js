ProductView = Backbone.View.extend({
	template:$('#template-product').html()
	,isLoaded:false
	,initialize:function(options){
		var that=this

		this.productModel = options.model
		this.$el = $(_.template(this.template,options.model.attributes))

		this.render()
	},render:function(){
		this.renderIsWanted();
		
		var that = this
			,images = this.productModel.get('images')
			,imgurImages = this.productModel.get('imgurImages')
			,srcs = []
			,$images = this.$el.find('.product-images')
			,$image = $images.find('.product-image')
			,$image0 = $image.eq(0)

		_.each(imgurImages,function(imgurImage,index){
			var image = images[index]

			if(!image){return}
			var extension = _.last(image.split('.'))
				,src= 'http://i.imgur.com/'+imgurImage+'.'+extension

			srcs.push(src)
		})

		this.$el.find('.product-display,.product-buttons .btn').tooltip({container:'body'})

		if(this.productModel.get('isHighlighted')){
			this.$el.addClass('product-highlighted')
		}
	
		new LazyView({}
			,$image0,srcs[0]
			,100*(that.productModel.collection.modelCount-1)
			,function(){
				$image0.addClass('product-image-loaded')
			}
		)

		this.$el.hover(function(){
			if(that.isLoaded){return}
			_.times(srcs.length-1,function(index){
				$image.eq(index+1)
					.css('background-image','url('+srcs[index+1]+')')
					.addClass('product-image-loaded')
			})
		})

		var $images = this.$el.find('.product-images')
		this.$el.find('.product-bullet').click(function(){
			var $this = $(this)
				scrollLeft = $this.index()*(that.$el.width()+5)
			$images.scrollLeft(scrollLeft)

			that.$el.find('.product-bullet-active').removeClass('product-bullet-active')
			$this.addClass('product-bullet-active')
		})

		var wants = this.productModel.get('wants')
			
		switch(wants) {
		    case 0:
		        var prettyWants = 'No Wants Yet'
		        break;
		    case 1:
		        var prettyWants = '1 Want'
		        break;
		    default:
		        var prettyWants = wants+' Wants'
		}
		
		this.$el.find('.product-wants').html(prettyWants)

		var price = this.productModel.get('price')
			,prettyPrice =
				(price==0 ? '':'$'+price+', ')
		
		this.$el.find('.product-price').html(prettyPrice)

		this.$el.find('.product-button-want').click(function(){
			that.productModel.set('isWanted',!that.productModel.get('isWanted'))
		})


	},renderIsWanted:function(){
		$wantButton = this.$el.find('.product-button-want')
		if(this.productModel.get('isWanted')){
			$wantButton.addClass('product-button-want-active')
		}else{
			$wantButton.removeClass('product-button-want-active')
		}
	}
});