BannerView = Backbone.View.extend({
	el:'.banner'
	,initialize:function(){
		var that = this
		$document.scroll(function(x){
			if($document.scrollTop()>100){
				that.$el.addClass('banner-active')
			}else{
				that.$el.removeClass('banner-active')
			}
		})
	}
})