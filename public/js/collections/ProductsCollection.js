ProductsCollection = Backbone.Collection.extend({
	model:ProductModel
	,initialize:function(models,$container){
		this.$container = $container;
		
		this.on('add',this.onAdd,this);
	},onAdd:function(productModel){
		this.$container.append(productModel.productView.$el)
	}
})